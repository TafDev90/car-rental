class AddColumnsToRental < ActiveRecord::Migration
  def change
    add_column :rentals, :odometer, :integer
    add_column :rentals, :fuel_level, :string
    add_column :rentals, :expected_pickup, :datetime
    add_column :rentals, :expected_dropoff, :datetime
  end
end
