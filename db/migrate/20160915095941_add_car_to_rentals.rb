class AddCarToRentals < ActiveRecord::Migration
  def change
    add_reference :rentals, :car, index: true, foreign_key: true
  end
end
