class Rental < ActiveRecord::Base
  # foreign_key is not needed here just because the names of the belongs_to fields
  # are EXACTLY the same as the foreign key names.
  belongs_to :car, inverse_of: :rentals
  # The customer
  belongs_to :customer, class_name: "User", inverse_of: :rented_cars
  # The employee who gives out the car
  belongs_to :clerk, class_name: "User", inverse_of: :given_cars
  # The employee with whom they drop back off
  belongs_to :collector, class_name: "User", inverse_of: :taken_cars

  validate :not_have_drop_off_time
  validate :cannot_create_rental
  validate :ensure_no_time_collision
  validate :is_in_the_clear

  private

  def not_have_drop_off_time
    if self.drop_off_time
      self.errors.add(:not_have_drop_off_time, "You cant include drop off time unless you are telling me you can see the future")
    end
  end

  def cannot_create_rental
    if self.collector
      self.errors.add(:cannot_create_rental, "You are not authorised to issue this rental")
    end
  end

  def ensure_no_time_collision
    unless self.car.is_available(self.pick_up_time, self.expected_dropoff)
      self.errors.add(:pick_up_time, "Timeframe collision")
    end
  end

  def is_in_the_clear

  end
end
