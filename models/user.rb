require 'bcrypt'

class User < ActiveRecord::Base
  has_many :user_roles
  has_many :roles, through: :user_roles
  has_many :rented_cars, class_name: "Rental", foreign_key: :customer_id, inverse_of: :customer
  has_many :given_cars, class_name: "Rental", foreign_key: :clerk_id, inverse_of: :clerk
  has_many :taken_cars, class_name: "Rental", foreign_key: :collector_id, inverse_of: :collector

  has_secure_password

  before_create :set_default_role

  def is_a(role_name)
    role = Role.find_by(name: role_name)
    self.user_roles.map(&:role_id).include? role.id
  end

  private

  def set_default_role
    unless self.roles.include? Role.find_by(name: "Customer")
      self.roles <<  Role.find_by(name: "Customer")
    end
  end

end
