class Car < ActiveRecord::Base
  has_many :rentals

  validate :is_available


  # Joe's cool algorithm to identify scheduling collisions
    def is_available(pu_time = Time.now, dr_time = Time.now)

      result = true
      # self.rentals.reload
      self.rentals.each do |rental|
        # Get either the actual drop off time, or if it's
        # nil then the anticipated drop off time
        # (If you have an actual_drop_off_time anyway)
        dropoff_time = rental.drop_off_time
        dropoff_time ||= rental.expected_dropoff if Time.now < rental.expected_dropoff
        result = false if (dr_time > rental.pick_up_time &&
            (dropoff_time.nil? || dropoff_time > pu_time))
      end
      result
    end
  end
