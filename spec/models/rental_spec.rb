require './spec/spec_helper'

describe Rental do
  before do
    extend SeedData
    @rental = Rental.create!(customer: @burns, clerk: @lisa, car: @mr_plow, odometer: 10000, fuel_level: "full",
                            pick_up_time: "2016-09-15 10:21:24", expected_dropoff: "2016-09-17 10:21:24")
    @valid_rental = Rental.new(customer: @homer, clerk: @lisa, car: @the_homer, odometer: 10000, fuel_level: "full",
                             pick_up_time: "2016-09-15 10:21:24", expected_dropoff: "2016-09-17 10:21:24")
  end

  context "as a clerk" do
    context "with valid rental information" do
      it "should be able to rent a car out to a customer" do
        expect(Rental.all).to include(@rental)
      end

      it "should have related customer and clerk" do
        expect(@rental.customer).to be(@burns)
        expect(@rental.clerk).to be(@lisa)
      end

      it "should be able to act as a customer to rent out a car from another clerk" do
        @rental1 = Rental.create(customer: @lisa, clerk: @carl, car: @mr_plow, odometer: 10000, fuel_level: "full")
        expect(Rental.all).to include(@rental)
      end

      it "should cause that the renting individual becomes a customer if they were not previously" do
        @rental = Rental.create(customer: @wiggum, clerk: @carl, car: @mr_plow, odometer: 10000, fuel_level: "full")
        expect(@wiggum.roles).to include(@customer)
      end
    end

    context "with valid rental information" do
      it "should not be valid for a creation with a collector" do
        bogus = Rental.create(customer: @wiggum, collector: @lenny, car: @mr_plow, odometer: 10000, fuel_level: "full")
        expect(bogus).to_not be_valid
      end

      it "should not be valid for creation with a drop-off time" do
        bogus = Rental.create(customer: @burns, clerk: @lisa, car: @mr_plow, odometer: 10000, fuel_level: "full",
                              drop_off_time: Time.now)
        expect(bogus).to_not be_valid
      end

      it "should not be able to rent out a car that is already rented out" do
        @mr_plow.rentals.reload
        rental2 = Rental.new(customer: @krusty, clerk: @lisa, car: @mr_plow, odometer: 10000, fuel_level: "full",
                                 pick_up_time:  "2016-09-15 16:21:24", expected_dropoff: "2016-09-16 16:21:24")
        expect(rental2).to_not be_valid
      end

      it "should not have expected drop off time before pickup time" do
        @valid_rental.expected_dropoff = @valid_rental.pick_up_time - 2.days
        expect(@valid_rental).to_not be_valid
      end

      it "should not be able to rent out an additional car to the same user" do
        # rental3 = Rental.new(customer: @burns, clerk: @lisa, car: @family_sedan, odometer: 10000, fuel_level: "full")
        #                      #pick_up_time:  "2016-09-15 16:21:24", expected_dropoff: "2016-09-16 16:21:24")
        # expect(rental3).to_not be_valid

      end

      it "should not be able to rent out a car to themselves" do

      end
    end
  end

  context "as a collector" do
    context "with a valid rental" do
      it "should be able to mark car as returned" do

      end

      it "should have a drop-off time indicated when accepting a car" do

      end
    end

    context "with invalid return information" do
      it "should not be able to return a car if odometer reading is absent" do

      end

      it "should not be able to return a car if the fuel level is absent" do

      end

      it "should not be able to accept a car rented out by themselves" do

      end
    end
  end
end