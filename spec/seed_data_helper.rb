module SeedData
  # When this module is extended into a class or test...
  def self.extended(object)
    # ... make instance variables available.
    object.instance_exec do


      UserRole.destroy_all
      User.destroy_all
      Role.destroy_all
      Rental.destroy_all
      Car.destroy_all

      @customer = Role.create(name: "Customer")
      @clerk = Role.create(name: "Clerk")
      @collector = Role.create(name: "Collector")
      @boss = Role.create(name: "Boss")

      @burns=User.create(name: "Charles Montgomery Burns")
      @roger_meyers=User.create(name: "Roger Meyers, Jr.")
      @krusty=User.create(name: "Krusty the Clown")
      @sideshow_bob=User.create(name: "Sideshow Bob")
      @sideshow_mel=User.create(name: "Sideshow Mel")
      @mr_teeny=User.create(name: "Mr. Teeny")
      @radioactive_man=User.create(name: "Radioactive Man")
      @bailey=User.create(name: "Mary Bailey")
      @smithers=User.create(name: "Waylon Smithers")
      @fat_tony=User.create(name: "Fat Tony")
      @quimby=User.create(name: "Mayor \"Diamond Joe\" Quimby")
      @wiggum=User.create(name: "Chief Clancy Wiggum")
      @lou=User.create(name: "Lou")
      @eddie=User.create(name: "Eddie")
      @lovejoy=User.create(name: "Reverend Timothy Lovejoy")
      @helen=User.create(name: "Helen Lovejoy")
      @carl=User.create(name: "Carl Carlson")
      @lenny=User.create(name: "Lenny Leonard")
      @marge=User.create(name: "Marge Simpson")
      @homer=User.create(name: "Homer Simpson")
      @bart=User.create(name: "Bart Simpson")
      @lisa=User.create(name: "Lisa Simpson")


      #
      # ['registered', 'banned', 'moderator', 'admin'].each do |role|
      #   Role.find_or_create_by({name: role})
      # end

      UserRole.new(user: @lisa, role: @clerk)
      UserRole.new(user: @lisa, role: @customer)
      UserRole.new(user: @carl, role: @clerk)
      UserRole.new(user: @burns, role: @customer)
      UserRole.new(user: @homer, role: @customer)
      UserRole.new(user: @lenny, role: @collector)
      UserRole.new(user: @helen, role: @collector)
      UserRole.new(user: @marge, role: @boss)
      UserRole.new(user: @fat_tony, role: @boss)




      # Vehicles
      # Vehicles
      @family_sedan = Car.create(year: 2012, make: "Yugo", model: "Family Sedan")
      @mr_plow = Car.create(year: 2013, make: "Skoda", model: "Mr. Plow")
      # @the_homer = Car.create(year: 2015, make: "Powell", model: "The Homer")
      # @soap_box_racer = Car.create(year: 2013, make: "Custom", model: "Soap Box Racer")
      # @canyonero = Car.create(year: 2012, make: "Powell", model: "Canyonero")
      # @elec_taurus = Car.create(year: 2014, make: "Ford", model: "Elec-Taurus")
      # @book_burning_mobile = Car.create(year: 2010, make: "Mercedes", model: "Book Burning Mobile")
      # @lil_bandit = Car.create(year: 1967, make: "Chevrolet", model: "Li'l Bandit")
      # #
    end
  end
end
