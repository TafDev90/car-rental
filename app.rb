require './config/environment'
require 'pry'
require 'sinatra/base'
# require 'rack-flash'
require 'sinatra'
require 'sinatra/reloader'
# require 'sinatra/flash'
require 'erb'

# enable :sessions
# use Rack::Flash

set :views, './views'

# Found the crazy secret by running:
#   bundle exec rake secret
use Rack::Session::Cookie, expire_after: 2592000,
  secret: "a861497d116d411c6455b18395739f9857035132fb08c307b4fae7c2e486a2b4981ac6d273a5d49fe2d4a193cff8a0e3ba9a852c057db84242abd3800b2d9f97"

def current_user
  User.find_by(id: session[:user_id])
end

# We ALWAYS get params (a hash), and it is built from:
#  1. Any :var thing in the path  (Like '/users/:id' would give :id)
#  2. Any incoming form parameter (<input name="fred"> would give :fred)
#  3. Anything on the querystring (http://localhost:4567/abc?def=ghi would give :def)


#################### USER CONTROLS #################################################################

get '/' do
  redirect to('/login')
end

get "/users" do
  @users = User.all.order(:name)
  erb :users_index
end

get "/login" do

  @users = User.all
  erb :user_login
end

get '/my_index' do
  user = User.find_by(params[:user])
  @roles = user.roles
  erb :my_index
end


post '/login' do
  user = User.find_by(params[:user])
  user = user.authenticate(params[:password])

  if user
    puts "Well Done"
    session[:user_id] = user.id.to_s
    redirect to '/my_index'
  else
    redirect to '/register'
  end
end

post '/logout' do
  current_user = nil
  session[:user_id] = nil
  # binding.pry
  redirect to '/login'
end


post '/submission' do
  user = User.create(params[:user])
  session[:user_id] = user.id.to_s
  redirect to('/login')
end

# Actually UPDATE the user
# post "/submission/:id" do
#   user = User.find(params[:id])
#   user.update(params[:user])
#   redirect to("/my_orders")
# end

get "/register" do
  @user = User.new
  # @roles = Role.all
  erb :user_registration
end
#
# get "/users/:id/edit" do
#   @user = User.find(params[:id])
#   @roles = Role.all.order(:name)
#   erb :users_edit
# end
#
# post "/users/:id" do
#   user = User.find(params[:id])
#   params[:user]["role_ids"] -= ["-1"]
#   user.update(params[:user])
#   redirect to("/users")
# end
#
# #################### USER CONTROLS  #################################################################

# #################### CAR CONTROLS  #################################################################


get '/new_car' do
  erb :new_car
end

post '/new_car' do
  @car = Car.create(params)
  redirect to '/cars_index'
end

get '/cars_index' do
  @cars = Car.all
  erb :cars_index
end

get "/cars/:id/edit" do
  car = Car.find(params[:id])
  # @users = User.all.order(:name)
  erb :cars_edit
end

#################### ROLES CONTROLS BEGIN #################################################################

get "/roles" do
  @roles = Role.all.order(:name)
  erb :roles_index
end

get "/roles/:id/edit" do
  @role = Role.find(params[:id])
  @users = User.all.order(:name)
  erb :roles_edit
end

post "/roles/:id" do
  role = Role.find(params[:id])
  params[:role]["user_ids"] -= ["-1"]
  role.update(params[:role])
  redirect to("/roles")
end

#################### ROLES CONTROLS #################################################################

#################### RENTAL CONTROLS #################################################################


get '/new_rental' do
  @cars = Car.all
  @customers = User.all
  erb :new_rental
end

post '/new_rental' do
  @rental = Rental.create(params[:rental].merge(
      expected_dropoff: DateTime.parse("#{params[:end_date]} #{params[:end_time]}"),
      pick_up_time: DateTime.parse("#{params[:start_date]} #{params[:start_time]}")
  ))
  redirect to '/rentals_index'

end

get '/rentals_index' do
  @rentals = Rental.all
  erb :rentals_index
end

#################### CLOSE DB CONNECTION #################################################################

after do
  ActiveRecord::Base.connection.close
end
