1) As an employee, i cannot rent a car to myself
2) As a customer, i cannot rent more than 1 car at the same time
3) As the employee, i should not be able to rent out cars that have already been rented out
4) As the manager, i want to see which of my employees rented out which cars and to who
5) As the manager, i want to see who received the car and at what time they received it
6) As an employee, i want to able to record the state of the car before i give it away and when i get it back
